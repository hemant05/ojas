<?php

/**
 * The Default Example Controller Class
 *
 * @author Hemant Mann
 */
use Framework\Controller as Controller;
use Framework\RequestMethods as RequestMethods;
use Framework\Registry as Registry;

class Home extends Controller {

    public function index() {
        
    }

    public function contact() {
    	$view = $this->getActionView();
        $conf = Registry::get("configuration");
        $mailto = $conf->parse("configuration/mail")->mail->account;

        $view->set("mailto", $mailto);

    	$fields = array("name", "email", "message");
    	$errors = array();
    	$action = RequestMethods::post("action");
    	if ($action) {
    	    foreach ($fields as $field) {
    	        $value = RequestMethods::post($field);
    	        if (empty($value)) {
    	            $errors[$field] = "** ". ucfirst($field). " is required";
    	        } else {
    	            $$field = $value;
    	        }
    	    }

    	    if (empty($errors)) {
    	    	$send = mail($mailto, "Query from ". $name, "Person Email: $email \r\n". $message);
    	    	if ($send === true) {
    	    	    $errors["sent"] = "Query sent! Thank you for your response";
    	    	} elseif ($send === false) {
    	    	    $errors["sent"] = "Failed to send message! Something went wrong Try again later";
    	    	}
    	    }
    	}
    	$view->set("errors", $errors);
    }

    public function courses() {

    }

    public function staff() {

    }

    public function admissions() {

    }

    public function about() {

    }

}
