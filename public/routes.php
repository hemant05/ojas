<?php

// define routes

$routes = array(
    array(
        "pattern" => "index",
        "controller" => "home",
        "action" => "index"
    ),
    array(
        "pattern" => "contact",
        "controller" => "home",
        "action" => "contact"
    ),
    array(
        "pattern" => "admissions",
        "controller" => "home",
        "action" => "admissions"
    ),
    array(
        "pattern" => "staff",
        "controller" => "home",
        "action" => "staff"
    ),
    array(
        "pattern" => "about",
        "controller" => "home",
        "action" => "about"
    ),array(
        "pattern" => "courses",
        "controller" => "home",
        "action" => "courses"
    )
);

// add defined routes
foreach ($routes as $route) {
    $router->addRoute(new Framework\Router\Route\Simple($route));
}

// unset globals
unset($routes);
